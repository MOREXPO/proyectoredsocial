# PETINLINE
Proyecto sustitutivo de  de fin de curso realizado por Iago Moreda

## Proyecto subido 1 
*15/05/2020*

Realice los tres primeros apartados del anteproyecto y le añadi una pantalla de la aplicación del móvil.El resto del tiempo me lo pase investigando como realizar el proyecto y cree el icono de la red social y su nombre.

## Proyecto subido 2 
*16/05/2020*

Añadi la pantalla de Registrarse de la aplicación movil y hice el modelo entidad-relación.

## Proyecto subido 3
*17/05/2020*

Rehice el modelo Entidad-Relación y lo transformé al modelo relacional,despues lo transformé en una base de datos real en el hosting de awardspace.

## Proyecto subido 4
*18/05/2020*

Hoy estuve investigando y probando hostings, ademas de distintas herramientas como laravel para poder hacer la apliación web.

## Proyecto subido 5
*19/05/2020*

Rediseñe mi modelo entidad-relación, así como transformarlo al modelo relacional. Además cree la aplicación web mediante laravel y lo conecte con mi hosting mysql y mediante las migraciones de larabel cree las tablas y por último les metí un sistema de autenticación.

## Proyecto subido 6
*20/05/2020*

Conseguí que los usuarios se puedan registrar y loguear en mi aplicación web, además cambie el modelo de la base de datos para incluirle el atributo de Nombre.

## Proyecto subido 7
*21/05/2020*

Realice consultas api para la comunicación con mi base de datos, cambie las rutas que venían por defecto en laravel y le metí una plantilla de bootstrap para mejorar la apariencia.

## Proyecto subido 8
*22/05/2020*

Estuve haciendo un formulario para añadir anuncios pero me estuvo fallando por lo que estuve todo el día con ello.

## Proyecto subido 9
*23/05/2020*

Logre realizar la pantalla para crear anuncios y para visualizarlos y esta totalmente funcional.

## Proyecto subido 10
*25/05/2020*

Realice el sistema de mensajería y comencé a desarrollar el sistema de anuncios favoritos.

## Proyecto subido 11
*26/05/2020*

Estuve haciendo pruebas y investigando sobre Jquery y javascript.

## Proyecto subido 12
*27/05/2020*

Logre realizar el checkbox de favoritos con ajax para agregarlos a la tabla y quitarlos,por lo que me pase todo el dia investigando ajax y jquery.

## Proyecto subido 13
*28/05/2020*

Consegui realizar mediante ajax que se me ponga el checkbox marcado si esta de favorito, ademas realice una vista para mostrar mis anuncios desde donde puedes eliminarlos,tambien una vista de mis anuncios favoritos.

## Proyecto subido 14
*29/05/2020*

Estuve investigando Laravel Passport y haciendo pruebas.

## Proyecto subido 15
*31/05/2020*

Inserte laravel Passport en mi proyecto para que solo puedas hacer consultas si estas autenticado con cierto token.

## Proyecto subido 16
*01/06/2020*

Realice una consulta api para la autenticación del usuario y le meti al proyecto android Retrofit para consumir el json.

## Proyecto subido 17
*02/06/2020*

Estuve haciendo pruebas y investigando para arreglar un error surgido al intentar acceder a la api.

## Proyecto subido 18
*03/06/2020*

Arregle el anterior error, surgido por culpa del emulador de android studio,además cree el activity de home y ya puedes iniciar sesión desde donde accedes a home.

## Proyecto subido 19
*04/06/2020*

Ya me funciona todo el sistema de autenticación, donde te puedes registrar y loguear.Hoy me surgieron variados errores al hacer consultas post con retrofit pero logre solucionarlos.

## Proyecto subido 20
*05/06/2020*

Realice el activity de home donde visualizas los anuncios y puedes acceder a ellos.Donde más trabajo me dio fue a la hora de recoger la imagen correspondiente del anuncio para visualizarla.

## Proyecto subido 21
*08/06/2020*

Hoy estuve arreglando un problema que tuve con el emulador de android studio que acabe reparando.Ademas cree el activity de crear anuncio y estuve investigando sobre el menu lateral.

## Proyecto subido 22
*09/06/2020*

Hoy he estado todo el día investigando e intentando subir una imagen a mi servidor de laravel mediante consulta post pero no fui capaz.Lo intente mediante Retrofit y mediante consulta directa con archivo PHP pero me fallo en todo.Esto lo dejaré para el final y seguiré haciendo otras cosas.

## Proyecto subido 23
*10/06/2020*

Cree el activity de mensajes y mis anuncios.El activity de mis anuncios ya es funcional por lo que se pueden eliminar anuncios desde ahí, mientras que el de mensajes solo aparecen en un recycler view pero no los puedes seleccionar para verlos.

## Proyecto subido 24
*11/06/2020*

Cree el activity de Favoritos donde aparecerán los anuncios que agregues en el Home con un switch, el cual añade y elimina el seguimiento del anuncio.

## Proyecto subido 25
*12/06/2020*

Cree el activity chat desde donde puedes enviar y recibir mensajes, además cree un modal en los anuncios para enviar mensajes desde ahí directamente.

## Proyecto subido 26
*15/06/2020*

Hoy finalmente remate lo de crear anuncios.Más o menos lo tengo terminado, pero lo estaré puliendo todo a partir de ahora.


## Proyecto subido 27
*16/06/2020*

Subí a mi hosting mi proyecto de laravel, y estuve intentando arreglar un problema que tengo con el móvil, puesto que no se me conecta con la API a causa del token.

## Proyecto subido 28
*17/06/2020*

Estuve arreglando el anteproyecto, haciendo la navegación de pantallas.
