package com.example.proyectoredsocialmovil;

import java.util.ArrayList;

import Model.Anuncios;

public class Sistema {
    private static String name,username,provincia,token;
    private static int idUsuario;

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        Sistema.token = token;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        Sistema.name = name;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        Sistema.username = username;
    }

    public static String getProvincia() {
        return provincia;
    }

    public static void setProvincia(String provincia) {
        Sistema.provincia = provincia;
    }

    public static int getIdUsuario() {
        return idUsuario;
    }

    public static void setIdUsuario(int idUsuario) {
        Sistema.idUsuario = idUsuario;
    }
}
