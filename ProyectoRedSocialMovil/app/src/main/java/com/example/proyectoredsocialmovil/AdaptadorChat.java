
package com.example.proyectoredsocialmovil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import Model.Mensajes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdaptadorChat extends RecyclerView.Adapter<AdaptadorChat.MyViewHolder> implements View.OnClickListener {
    ArrayList<Mensajes> mensajes; // puede ser cualquier estructura de datos
    int selectedPos = RecyclerView.NO_POSITION;
    private View.OnClickListener listener;
    RecyclerView rv;
    Context context;

    public void setOnClickListener(View.OnClickListener listener) { // el nombre del método es indiferente
        this.listener = listener;
    }

    // Constructor
    public AdaptadorChat(ArrayList<Mensajes> mensajes, RecyclerView rv, Context context) {
        this.mensajes = mensajes;
        this.rv = rv;
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        int aux = rv.getChildAdapterPosition(v);
        marcoPosicion(aux);
        if (listener != null) listener.onClick(v);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // Elementos que queremos mostrar en el RecyclerView, normalmente se corresponderán
// con los definidos en el layout
        private TextView txtUsuario;

        // Constructor: asocia cada atributo de la clase con su correspondiente en el layout definido por View
        public MyViewHolder(View viewElemento) {
            super(viewElemento);
            this.txtUsuario = viewElemento.findViewById(R.id.txtUsuario);
        }
    }


    // Crea nuevos elementos expandiendo el layout definido en el fichero R.layout.elemento_individual
    // que usamos para crear el Holder que devolveremos
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View elemento = LayoutInflater.from(parent.getContext()).inflate(R.layout.celda_chat,
                parent, false);
        elemento.setOnClickListener(this);
        MyViewHolder mvh = new MyViewHolder(elemento);
        return mvh;
    }

    // Sustituye el contenido del elemento definido por holder por los valores de la
    // colección de datos que están en la posición position.
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Mensajes so = this.mensajes.get(position);
        if(so.getIdUsuarioEmisor()==Sistema.getIdUsuario()){
            holder.itemView.setBackgroundResource(R.color.verde);
        }else{
            holder.itemView.setBackgroundResource(R.color.azul);
        }
        holder.txtUsuario.setText(so.getTexto());

    }

    // Número de elementos en la colección de datos
    @Override
    public int getItemCount() {
        return this.mensajes.size();
    }

    public int getSelectedPos() {
        return selectedPos;
    }

    public void desmarcoPosicion() {
        int aux = this.selectedPos;
        this.selectedPos = RecyclerView.NO_POSITION;
        if (aux >= 0) notifyItemChanged(aux);
    }

    public void marcoPosicion(int nuevaPos) {
        if (selectedPos >= 0) notifyItemChanged(selectedPos);
        this.selectedPos = nuevaPos;
        notifyItemChanged(selectedPos);
    }
}


