package com.example.proyectoredsocialmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Model.Mensajes;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Chat extends AppCompatActivity {
    final long EXECUTION_TIME = 1000;
    RecyclerView rv;
    private Handler handler;
    EditText etEnviar;
    Button btnEnviar;
    Context context;
    Sistema system;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        system = new Sistema();
        intent=getIntent();
        context = this;
        etEnviar=findViewById(R.id.etEnviarMensaje);
        btnEnviar=findViewById(R.id.btnEnviarMensaje);

        getChat();
        handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {

                getChat();

                handler.postDelayed(this, EXECUTION_TIME);
            }
        }, EXECUTION_TIME);
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ResponseBody> call = RetrofitClient
                        .getRetrofitClient()
                        .getApi()
                        .enviarMensaje(Sistema.getIdUsuario(),intent.getIntExtra("idAnuncio",0),etEnviar.getText().toString());
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                        etEnviar.setText("");
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
    public void getChat() {
        final ArrayList<Mensajes> mensajesArrayLists = new ArrayList<>();
        Call<List<Mensajes>> call = RetrofitClient
                .getRetrofitClient()
                .getApi()
                .getChat(intent.getIntExtra("idAnuncio",0));
        call.enqueue(new Callback<List<Mensajes>>() {

            @Override
            public void onResponse(Call<List<Model.Mensajes>> call, Response<List<Mensajes>> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(context, "Código:" + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                List<Model.Mensajes> anunciosList = response.body();

                for (Model.Mensajes mensaje : anunciosList) {
                    mensajesArrayLists.add(mensaje);
                }
                rv = findViewById(R.id.rvChat);
                AdaptadorChat adaptadorChat = new AdaptadorChat(mensajesArrayLists, rv,context);
                RecyclerView.LayoutManager miLayoutManager;
                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                };
                adaptadorChat.setOnClickListener(listener);
                miLayoutManager = new LinearLayoutManager(context,  GridLayoutManager.VERTICAL, false);
                ((LinearLayoutManager) miLayoutManager).setStackFromEnd(true);
                rv.setLayoutManager(miLayoutManager);
                rv.setAdapter(adaptadorChat);
            }

            @Override
            public void onFailure(Call<List<Model.Mensajes>> call, Throwable t) {
                Toast.makeText(context, "ERROR:" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}