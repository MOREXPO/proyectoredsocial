package com.example.proyectoredsocialmovil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import Model.Users;
import okhttp3.Authenticator;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.Route;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class crearAnuncio extends AppCompatActivity {
    private EditText etNombre, etDescripcion;
    private Button btnSeleccionarArchivo, btnPublicar;
    Context context;
    Uri uri;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_anuncio);
        context = this;
        etNombre = findViewById(R.id.etNombre);
        etDescripcion = findViewById(R.id.etDescripcion);
        btnSeleccionarArchivo = findViewById(R.id.btnSeleccionarArchivo);
        btnPublicar = findViewById(R.id.btnPublicar);
        btnSeleccionarArchivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnPublicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etDescripcion.getText().toString().isEmpty() || etNombre.getText().toString().isEmpty()) {
                    Toast.makeText(context, "Un campo está vacío", Toast.LENGTH_LONG).show();
                } else if (uri == null) {
                    Toast.makeText(context, "No seleccionaste una imagen", Toast.LENGTH_LONG).show();
                } else {

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu); // se expande el menú y se añade a la ActionBar
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemCerrarSesion:
                intent = new Intent(context, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.itemCrearAnuncio:
                return true;
            case R.id.itemMisAnuncios:
                intent = new Intent(context, MisAnuncios.class);
                startActivity(intent);
                return true;
            case R.id.itemAnuncios:
                intent = new Intent(context, Home.class);
                startActivity(intent);
                return true;
            case R.id.itemMensajes:
                intent = new Intent(context, Mensajes.class);
                startActivity(intent);
                return true;
            case R.id.itemFavoritos:
                intent = new Intent(context, Favoritos.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            Toast.makeText(this, "URL:" + imageUri, Toast.LENGTH_LONG).show();
        }
    }

    public void UploadImage() {
        File file = new File(FileUtil.getPath(imageUri, this));
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part parts = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"),Sistema.getIdUsuario()+"");
        RequestBody titulo = RequestBody.create(MediaType.parse("text/plain"),etNombre.getText().toString());
        RequestBody descripcion = RequestBody.create(MediaType.parse("text/plain"),etDescripcion.getText().toString());
        Call call = RetrofitClient
                .getRetrofitClient()
                .getApi()
                .createAnuncio(parts,id,titulo,descripcion);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, retrofit2.Response response) {
            }

            @Override
            public void onFailure(Call call, Throwable t) {
            }
        });
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

}