package com.example.proyectoredsocialmovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import Model.Users;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private Button btnRegister, btnAcceder;
    private EditText txtNombre, txtPassword;
    private Intent intent;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnAcceder = findViewById(R.id.btnAcceder);
        btnRegister = findViewById(R.id.btnRegister);
        txtNombre = findViewById(R.id.txtNombre);
        txtPassword = findViewById(R.id.txtPassword);
        context = this;
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context, Registro.class);
                startActivity(intent);
            }
        });
        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autenticar();
            }
        });
    }

    public void autenticar() {
        Call<List<Users>> call = RetrofitClient
                .getRetrofitClient()
                .getApi()
                .getUser(txtNombre.getText().toString(), txtPassword.getText().toString());
        call.enqueue(new Callback<List<Users>>() {
            @Override
            public void onResponse(Call<List<Users>> call, Response<List<Users>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(context, "Código:" + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                List<Users> usersList = response.body();

                for (Users user : usersList) {
                    Sistema.setIdUsuario(user.getId());
                    Sistema.setName(user.getName());
                    Sistema.setUsername(user.getUsername());
                    Sistema.setProvincia(user.getProvincia());
                }
                intent = new Intent(context, Home.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<List<Users>> call, Throwable t) {
                Toast.makeText(context, "ERROR:" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
