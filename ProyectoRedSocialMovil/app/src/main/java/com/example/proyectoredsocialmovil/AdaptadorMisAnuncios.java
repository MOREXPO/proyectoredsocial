
package com.example.proyectoredsocialmovil;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import Model.Anuncios;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdaptadorMisAnuncios extends RecyclerView.Adapter<AdaptadorMisAnuncios.MyViewHolder> implements View.OnClickListener{
    ArrayList<Anuncios> anuncios; // puede ser cualquier estructura de datos
    int selectedPos=RecyclerView.NO_POSITION;
    private View.OnClickListener listener;
    RecyclerView rv;
    Context context;
    public void setOnClickListener(View.OnClickListener listener) { // el nombre del método es indiferente
        this.listener = listener;
    }
    // Constructor
    public AdaptadorMisAnuncios(ArrayList<Anuncios> anuncios, RecyclerView rv,Context context) {
        this.anuncios = anuncios;
        this.rv=rv;
        this.context=context;
    }

    @Override
    public void onClick(View v) {
        int aux=rv.getChildAdapterPosition(v);
        marcoPosicion(aux);
        if(listener != null) listener.onClick(v);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // Elementos que queremos mostrar en el RecyclerView, normalmente se corresponderán
// con los definidos en el layout
        private TextView titulo;
        ImageView foto;
        Button btnEliminar;

        // Constructor: asocia cada atributo de la clase con su correspondiente en el layout definido por View
        public MyViewHolder(View viewElemento) {
            super(viewElemento);
            this.titulo = viewElemento.findViewById(R.id.txtTitulo);
            this.foto = viewElemento.findViewById(R.id.foto);
            this.btnEliminar = viewElemento.findViewById(R.id.btnEliminar);
        }
    }


    // Crea nuevos elementos expandiendo el layout definido en el fichero R.layout.elemento_individual
    // que usamos para crear el Holder que devolveremos
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View elemento = LayoutInflater.from(parent.getContext()).inflate(R.layout.celda_mis_anuncios,
                parent, false);
        elemento.setOnClickListener(this);
        MyViewHolder mvh = new MyViewHolder(elemento);
        return mvh;
    }

    // Sustituye el contenido del elemento definido por holder por los valores de la
    // colección de datos que están en la posición position.
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final Anuncios so = this.anuncios.get(position);
        holder.titulo.setText(so.getTitulo());
        new LoadImage(holder.foto).execute("http://10.0.3.2:8000/images/"+so.getFoto());
        holder.itemView.setBackgroundResource(R.color.elemento);
        holder.btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ResponseBody> call = RetrofitClient
                        .getRetrofitClient()
                        .getApi()
                        .deleteAnuncio(so.getId());
                call.enqueue(new Callback<ResponseBody>() {

                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(context, "Código:" + response.code(), Toast.LENGTH_LONG).show();
                            return;
                        }
                        eliminarPosicion(position);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(context, "ERROR:" + t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    // Número de elementos en la colección de datos
    @Override
    public int getItemCount() {
        return this.anuncios.size();
    }

    public int getSelectedPos() {
        return selectedPos;
    }

    public void desmarcoPosicion(){
        int aux=this.selectedPos;
        this.selectedPos=RecyclerView.NO_POSITION;
        if (aux>=0) notifyItemChanged(aux);
    }

    public void marcoPosicion(int nuevaPos){
        if (selectedPos>=0) notifyItemChanged(selectedPos);
        this.selectedPos=nuevaPos;
        notifyItemChanged(selectedPos);
    }

    public void eliminarPosicion(int pos){
        anuncios.remove(pos);
        notifyItemRemoved(pos);
    }
}


