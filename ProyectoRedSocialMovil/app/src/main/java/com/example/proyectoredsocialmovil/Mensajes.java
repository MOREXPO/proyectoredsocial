package com.example.proyectoredsocialmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Mensajes extends AppCompatActivity {
    RecyclerView rv;
    Context context;
    int pos;
    Sistema system;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensajes);
        system = new Sistema();
        context = this;
        getMensajes();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu); // se expande el menú y se añade a la ActionBar
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemCerrarSesion:
                intent = new Intent(context, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.itemCrearAnuncio:
                intent = new Intent(context, crearAnuncio.class);
                startActivity(intent);
                return true;
            case R.id.itemMisAnuncios:
                intent = new Intent(context, MisAnuncios.class);
                startActivity(intent);
                return true;
            case R.id.itemAnuncios:
                intent = new Intent(context, Home.class);
                startActivity(intent);
                return true;
            case R.id.itemMensajes:
                return true;
            case R.id.itemFavoritos:
                intent = new Intent(context, Favoritos.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getMensajes() {
        final ArrayList<Model.Mensajes> mensajesArrayLists = new ArrayList<>();
        Call<List<Model.Mensajes>> call = RetrofitClient
                .getRetrofitClient()
                .getApi()
                .getMensajes(system.getIdUsuario());
        call.enqueue(new Callback<List<Model.Mensajes>>() {

            @Override
            public void onResponse(Call<List<Model.Mensajes>> call, Response<List<Model.Mensajes>> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(context, "Código:" + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                List<Model.Mensajes> anunciosList = response.body();

                for (Model.Mensajes mensaje : anunciosList) {
                    mensajesArrayLists.add(mensaje);
                }
                rv = findViewById(R.id.rvMensajes);
                AdaptadorMensajes adaptadorMensajes = new AdaptadorMensajes(mensajesArrayLists, rv,context);
                RecyclerView.LayoutManager miLayoutManager;
                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pos=rv.getChildAdapterPosition(view); // pos es un atributo de instancia de MainActivity
                        intent=new Intent(context,Chat.class);
                        intent.putExtra("idAnuncio",mensajesArrayLists.get(pos).getIdAnuncio());
                        startActivity(intent);
                    }
                };
                adaptadorMensajes.setOnClickListener(listener);
                miLayoutManager = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
                rv.setLayoutManager(miLayoutManager);
                rv.setAdapter(adaptadorMensajes);
            }

            @Override
            public void onFailure(Call<List<Model.Mensajes>> call, Throwable t) {
                Toast.makeText(context, "ERROR:" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}