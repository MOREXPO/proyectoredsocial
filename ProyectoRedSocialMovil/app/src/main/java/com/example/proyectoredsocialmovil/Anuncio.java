package com.example.proyectoredsocialmovil;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Anuncio extends AppCompatActivity {
    Intent intent, intent2;
    TextView txtTitulo, txtDescripcion;
    Button btnChat;
    ImageView foto;
    String imageURL = "http://10.0.3.2:8000/images/";
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anuncio);
        context = this;
        txtTitulo = findViewById(R.id.txtTitulo);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        foto = findViewById(R.id.fotoAnuncio);
        btnChat = findViewById(R.id.btnChat);
        intent = getIntent();
        txtTitulo.setText(intent.getStringExtra("titulo"));
        txtDescripcion.setText(intent.getStringExtra("descripcion"));
        imageURL += intent.getStringExtra("foto");
        new LoadImage(foto).execute(imageURL);
        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Anuncio.this);
                alertDialog.setTitle(intent.getStringExtra("titulo"));
                alertDialog.setMessage(R.string.IntroducirMensaje);

                final EditText input = new EditText(Anuncio.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input);
                alertDialog.setPositiveButton(R.string.Enviar,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Call<ResponseBody> call = RetrofitClient
                                        .getRetrofitClient()
                                        .getApi()
                                        .enviarMensaje(Sistema.getIdUsuario(), intent.getIntExtra("id", 0), input.getText().toString());
                                call.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                                        input.setText("");
                                        intent2 = new Intent(context, Mensajes.class);
                                        startActivity(intent2);
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });

                alertDialog.setNegativeButton(R.string.Salir,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                alertDialog.show();
            }

        });
    }
}
