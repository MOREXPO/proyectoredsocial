package com.example.proyectoredsocialmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Model.Anuncios;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Favoritos extends AppCompatActivity {
    RecyclerView rv;
    Context context;
    int pos;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoritos);
        context = this;
        getAnunciosFavoritos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu); // se expande el menú y se añade a la ActionBar
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemCerrarSesion:
                intent = new Intent(context, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.itemCrearAnuncio:
                intent = new Intent(context, crearAnuncio.class);
                startActivity(intent);
                return true;
            case R.id.itemMisAnuncios:
                intent = new Intent(context, MisAnuncios.class);
                startActivity(intent);
                return true;
            case R.id.itemAnuncios:
                intent = new Intent(context, Home.class);
                startActivity(intent);
                return true;
            case R.id.itemMensajes:
                intent = new Intent(context, Mensajes.class);
                startActivity(intent);
                return true;
            case R.id.itemFavoritos:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void getAnunciosFavoritos() {
        final ArrayList<Anuncios> anunciosArrayLists = new ArrayList<>();
        Call<List<Anuncios>> call = RetrofitClient
                .getRetrofitClient()
                .getApi()
                .anunciosFavoritos(Sistema.getIdUsuario());
        call.enqueue(new Callback<List<Anuncios>>() {

            @Override
            public void onResponse(Call<List<Anuncios>> call, Response<List<Anuncios>> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(context, "Código:" + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                List<Anuncios> anunciosList = response.body();

                for (Anuncios anuncio : anunciosList) {
                    anunciosArrayLists.add(anuncio);
                }
                rv = findViewById(R.id.rvFavoritos);
                AdaptadorHome adaptadorHome = new AdaptadorHome(anunciosArrayLists, rv,context);
                RecyclerView.LayoutManager miLayoutManager;
                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pos = rv.getChildAdapterPosition(view); // pos es un atributo de instancia de MainActivity
                        intent = new Intent(context, Anuncio.class);
                        intent.putExtra("id", anunciosArrayLists.get(pos).getId());
                        intent.putExtra("titulo", anunciosArrayLists.get(pos).getTitulo());
                        intent.putExtra("descripcion", anunciosArrayLists.get(pos).getDescripcion());
                        intent.putExtra("foto", anunciosArrayLists.get(pos).getFoto());
                        intent.putExtra("idUsuarioPublicador", anunciosArrayLists.get(pos).getIdUsuarioPublicador());
                        startActivity(intent);
                    }
                };
                adaptadorHome.setOnClickListener(listener);
                miLayoutManager = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
                rv.setLayoutManager(miLayoutManager);
                rv.setAdapter(adaptadorHome);
            }

            @Override
            public void onFailure(Call<List<Anuncios>> call, Throwable t) {
                Toast.makeText(context, "ERROR:" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}