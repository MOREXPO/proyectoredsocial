package com.example.proyectoredsocialmovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import Model.Users;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Registro extends AppCompatActivity {
    private Button btnVolver, btnRegistrar;
    private EditText txtNombre, txtUsername, txtPassword, txtConfimPassword, txtProvincia;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        context = this;
        txtNombre = findViewById(R.id.txtNombre);
        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        txtConfimPassword = findViewById(R.id.txtConfirmPassword);
        txtProvincia = findViewById(R.id.txtProvincia);
        btnVolver = findViewById(R.id.btnVolver);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNombre.getText().toString().isEmpty()||txtUsername.getText().toString().isEmpty()||txtPassword.getText().toString().isEmpty()||txtConfimPassword.getText().toString().isEmpty()||txtProvincia.getText().toString().isEmpty()){
                    Toast.makeText(context,"Algún campo está vacío",Toast.LENGTH_LONG).show();
                }else if(!txtPassword.getText().toString().equalsIgnoreCase(txtConfimPassword.getText().toString())){
                    Toast.makeText(context,"Las contraseñas no coinciden",Toast.LENGTH_LONG).show();
                }else{
                    register();
                }

            }
        });
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void register() {

        Call<ResponseBody> call = RetrofitClient
                .getRetrofitClient()
                .getApi()
                .register(txtNombre.getText().toString(), txtUsername.getText().toString(), txtPassword.getText().toString(), txtProvincia.getText().toString());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                Toast.makeText(context,"Usuario registrado con éxito",Toast.LENGTH_LONG).show();
                onBackPressed();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
