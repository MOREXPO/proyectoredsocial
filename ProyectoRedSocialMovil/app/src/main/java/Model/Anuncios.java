package Model;

public class Anuncios {
    private int id;
    private String titulo,foto,descripcion;
    private int idUsuarioPublicador;
    private String created_at,updated_at;

    public Anuncios(int id, String titulo, String foto, String descripcion, int idUsuarioPublicador, String created_at, String updated_at) {
        this.id = id;
        this.titulo = titulo;
        this.foto = foto;
        this.descripcion = descripcion;
        this.idUsuarioPublicador = idUsuarioPublicador;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdUsuarioPublicador() {
        return idUsuarioPublicador;
    }

    public void setIdUsuarioPublicador(int idUsuarioPublicador) {
        this.idUsuarioPublicador = idUsuarioPublicador;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
