package Model;

public class Mensajes {
    private int id;
    private String texto;
    private int idUsuarioEmisor,idAnuncio;
    private String created_at,updated_at;

    public Mensajes(int id, String texto, int idUsuarioEmisor, int idAnuncio, String created_at, String updated_at) {
        this.id = id;
        this.texto = texto;
        this.idUsuarioEmisor = idUsuarioEmisor;
        this.idAnuncio = idAnuncio;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getIdUsuarioEmisor() {
        return idUsuarioEmisor;
    }

    public void setIdUsuarioEmisor(int idUsuarioEmisor) {
        this.idUsuarioEmisor = idUsuarioEmisor;
    }

    public int getIdAnuncio() {
        return idAnuncio;
    }

    public void setIdAnuncio(int idAnuncio) {
        this.idAnuncio = idAnuncio;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
