<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seguir extends Model
{
    protected $fillable=["idUsuario","idAnuncio"];
}
