<?php

namespace App\Http\Controllers;

use App\Models\Seguir;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Environment\Console;
use Symfony\Component\Console\Input\Input;

class SeguirController extends Controller
{
    public function inicio(){
        $anuncios = DB::select('SELECT * FROM anuncios WHERE id IN (SELECT idAnuncio FROM seguirs where idUsuario=?)', [auth()->id()]);
        return view("anuncios.favoritos", compact("anuncios"));
    }
    public function destroy($idAnuncio)
    {
        Seguir::where('idUsuario', auth()->id())->where('idAnuncio', $idAnuncio)->delete();
    }

    public function store($idAnuncio)
    {
        $seguimiento = new Seguir();
        $seguimiento->idUsuario = auth()->id();
        $seguimiento->idAnuncio = $idAnuncio;
        $seguimiento->save();
    }

    public function verificar($idAnuncio)
    {
        $response = array(
            'status' => 'success',
            'exist' => Seguir::where('idUsuario', '=', auth()->id())->where('idAnuncio','=',$idAnuncio)->exists(),
        );
        return response()->json($response); 
    }
}
