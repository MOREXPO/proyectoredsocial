<?php

namespace App\Http\Controllers;

use App\Models\Anuncio;
use App\Models\Mensaje;
use App\Models\Seguir;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class APIConsultasController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('client-credentials');
    }


    public function getAnunciosHome($idUsuario){
        $anuncios = Anuncio::where('idUsuarioPublicador','!=',$idUsuario)->get();
        return $anuncios;
    }

    public function getMisAnuncios($idUsuario){
        $anuncios = DB::select('SELECT * FROM anuncios WHERE idUsuarioPublicador=?',[$idUsuario]);
        return $anuncios;
    }

    public function getMensajes($idUsuario){
        $mensajes = DB::select('SELECT * FROM mensajes WHERE idAnuncio IN (SELECT id FROM anuncios where idUsuarioPublicador=?) or idUsuarioEmisor=? group by idAnuncio', [$idUsuario,$idUsuario]);
        return $mensajes;
    }

    public function deleteAnuncio($idAnuncio)
    {
        $anuncio = Anuncio::find($idAnuncio);
        $favoritosAnuncio=DB::delete('delete from seguirs where idAnuncio = ?',[$idAnuncio]);
        $mensajesAnuncio=DB::delete('delete from mensajes where idAnuncio = ?',[$idAnuncio]);
        $anuncio->delete();
    }

    public function createUser($name,$username,$password,$provincia)
    {
        $user = new User();
        $user->name = $name;
        $user->username = $username;
        $user->password = Hash::make($password);
        $user->provincia = $provincia;
        $user->save();
    }

    public function createAnuncio(Request $request)
    {
        $entrada = $request->all();
        if ($archivo = $request->file('file')) {
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('images', $nombre);
            $entrada['foto'] = $nombre;
        }
        Anuncio::create([
            'titulo'    => $entrada['titulo'],
            'descripcion' => $entrada['descripcion'],
            'foto'         => $entrada['foto'],
            'idUsuarioPublicador'         => auth()->id(),
        ]);
    }

    public function nombreMensajero($idUsuarioEmisor)
    {
        $array = json_decode(User::where('id', $idUsuarioEmisor)->get('username'));
        return $array[0]->username;
    }

    public function nombreUsuarioDestino($idAnuncio)
    {
        $idUsuarioPublicador=json_decode(Anuncio::where('id', $idAnuncio)->get('idUsuarioPublicador'));
        $array = json_decode(User::where('id', $idUsuarioPublicador[0]->idUsuarioPublicador)->get('username'));
        return $array[0]->username;
    }

    public static function obtenerImagen($idAnuncio)
    {
       
        $array = json_decode(Anuncio::where('id', $idAnuncio)->get('foto'));
        return $array[0]->foto;
    }

    public function eliminarSeguir($idUsuario,$idAnuncio)
    {
        Seguir::where('idUsuario', $idUsuario)->where('idAnuncio', $idAnuncio)->delete();
    }

    public function agregarSeguir($idUsuario,$idAnuncio)
    {
        $seguimiento = new Seguir();
        $seguimiento->idUsuario = $idUsuario;
        $seguimiento->idAnuncio = $idAnuncio;
        $seguimiento->save();
    }

    public function verificar($idUsuario,$idAnuncio)
    {
        if(Seguir::where('idUsuario', '=', $idUsuario)->where('idAnuncio','=',$idAnuncio)->exists()){
            return "true";
        }else{
            return "false";
        }
    }

    public function anunciosFavoritos($idUsuario){
        $anuncios = DB::select('SELECT * FROM anuncios WHERE id IN (SELECT idAnuncio FROM seguirs where idUsuario=?)', [$idUsuario]);
        return $anuncios;
    }

    public function getChat($idAnuncio){
        $mensajesAnuncio = DB::select('SELECT * FROM mensajes WHERE idAnuncio = ?',[$idAnuncio]);
        return $mensajesAnuncio;
    }

    public function enviarMensaje($idUsuario,$idAnuncio,$texto)
    {
        Mensaje::create([
            'texto'    => $texto,
            'idAnuncio' => $idAnuncio,
            'idUsuarioEmisor'         => $idUsuario,
        ]);
    }

    public function Validacion($username, $password) {
        $passwordAux = User::where('username', 'like', $username)->get('password');
        $passwordAux=json_decode($passwordAux,true);
        $passwordAux=$passwordAux[0]['password'];
        if (Hash::check($password, $passwordAux)) {
            return User::where('username', 'like', $username)->get();
        } else {
            return "false";
        }
    
    }
}


