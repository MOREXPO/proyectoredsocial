<?php

namespace App\Http\Controllers;

use App\Models\Anuncio;
use Carbon\Carbon;
use CreateUsersTable;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('anuncios.home');
    }

    public function inicio(){
        $anuncios = Anuncio::where('idUsuarioPublicador','!=',auth()->id())->get();
        return view("anuncios.home", compact("anuncios"));
    }


}
