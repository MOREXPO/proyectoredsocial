<?php

namespace App\Http\Controllers;

use App\Models\Anuncio;
use App\Models\Mensaje;
use App\Models\Seguir;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AnuncioController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $anuncios = \App\Models\Anuncio::all();
        return $anuncios;
    }

    public function store(Request $request)
    {
        $this->validate($request,['file'=>'required|image|mimes:jpeg,png,jpg']);
        $entrada = $request->all();
        if ($archivo = $request->file('file')) {
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('images', $nombre);
            $entrada['foto'] = $nombre;
        }
        Anuncio::create([
            'titulo'    => $entrada['titulo'],
            'descripcion' => $entrada['descripcion'],
            'foto'         => $entrada['foto'],
            'idUsuarioPublicador'         => auth()->id(),
        ]);
        return redirect()->route('home');
    }
    public function inicio(){
        $anuncios = DB::select('SELECT * FROM anuncios WHERE idUsuarioPublicador=?', [auth()->id()]);
        return view("anuncios.misAnuncios", compact("anuncios"));
    }
    public function destroy($id)
    {
        $anuncio = Anuncio::find($id);
        $favoritosAnuncio=DB::delete('delete from seguirs where idAnuncio = ?',[$id]);
        $mensajesAnuncio=DB::delete('delete from mensajes where idAnuncio = ?',[$id]);
        $anuncio->delete();
 
        return redirect()->route('mis-anuncios');
    }

    public function show($id)
    {
        $anuncio = \App\Models\Anuncio::find($id);
        return $anuncio;
    }

    public function showPublicador($idPublicador)
    {
        $anuncio = \App\Models\Anuncio::where('idUsuarioPublicador', $idPublicador)->get();
        return $anuncio;
    }

    public function create()
    {
        return view('anuncios.create');
    }

    public static function nombrePublicador($idUsuarioPublicador)
    {
        $array = json_decode(User::where('id', $idUsuarioPublicador)->get('username'));
        return $array[0]->username;
    }
}
