<?php

namespace App\Http\Controllers;

use App\Models\Anuncio;
use App\Models\Mensaje;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MensajeController extends Controller
{
    public function inicio(){
        $mensajes = DB::select('SELECT * FROM mensajes WHERE idAnuncio IN (SELECT id FROM anuncios where idUsuarioPublicador=?) or idUsuarioEmisor=? group by idAnuncio', [auth()->id(),auth()->id()]);
        return view("mensajes.mensajes", compact("mensajes"));
    }

    public function store(Request $request,$idAnuncio)
    {
        $this->validate($request,['texto'=>'required']);
        $entrada = $request->all();
        Mensaje::create([
            'texto'    => $entrada['texto'],
            'idAnuncio' => $idAnuncio,
            'idUsuarioEmisor'         => auth()->id(),
        ]);
        return redirect()->route('mensajes');
    }

    public static function nombreMensajero($idUsuarioEmisor)
    {
        $array = json_decode(User::where('id', $idUsuarioEmisor)->get('username'));
        return $array[0]->username;
    }

    public static function nombreUsuarioDestino($idAnuncio)
    {
        $idUsuarioPublicador=json_decode(Anuncio::where('id', $idAnuncio)->get('idUsuarioPublicador'));
        $array = json_decode(User::where('id', $idUsuarioPublicador[0]->idUsuarioPublicador)->get('username'));
        return $array[0]->username;
    }
}
