<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeguirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguirs', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->foreignId('idUsuario')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
            
            $table->foreignId('idAnuncio')->references('id')->on('anuncios')->onDelete('restrict')->onUpdate('cascade');
            
            $table->primary(['idUsuario', 'idAnuncio']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seguirs');
    }
}
