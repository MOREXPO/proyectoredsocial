@extends('layouts.app')

@section('content')
<!-- Portfolio Section-->
<section class="page-section portfolio" id="portfolio">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Mis anuncios</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Portfolio Grid Items-->
        <div class="row">

            @forelse($anuncios as $anuncio)
            <div class="col-md-6 col-lg-4 mb-5">
                <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal{{$anuncio->id}}">
                    <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                        <div class="portfolio-item-caption-content text-center text-white"><i
                                class="fas fa-plus fa-3x"></i></div>
                    </div>
                    <img class="img-fluid" src="images/{{$anuncio->foto}}" alt="" />
                </div>
            </div>
            @empty

            @endforelse
        </div>
    </div>
</section>
<!-- Portfolio Modals-->
@forelse($anuncios as $anuncio)
<div class="portfolio-modal modal fade" id="portfolioModal{{$anuncio->id}}" tabindex="-1" role="dialog"
    aria-labelledby="portfolioModal{{$anuncio->id}}Label" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fas fa-times"></i></span>
            </button>
            <div class="nombrePublicador text-secondary text-uppercase mb-0">
                @php
                $idAnuncio=$anuncio->id;
                @endphp
            </div>
            <div class="idAnuncio" style="display: none">{{$anuncio->id}}</div>
            <div class="modal-body text-center">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <!-- Portfolio Modal - Title-->
                            <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0"
                                id="portfolioModal{{$anuncio->id}}Label">{{$anuncio->titulo}}</h2>
                            <!-- Icon Divider-->
                            <div class="divider-custom">
                                <div class="divider-custom-line"></div>
                                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                <div class="divider-custom-line"></div>
                            </div>
                            <!-- Portfolio Modal - Image--><img class="img-fluid rounded mb-5"
                                src="images/{{$anuncio->foto}}" alt="" /><!-- Portfolio Modal - Text-->
                            <p class="mb-5">{{$anuncio->descripcion}}
                            </p>
                            <form method="POST" action="{{ url("eliminar-anuncio/{$anuncio->id}") }}">
                                @csrf
                                @method('DELETE')

                                <button class="btn btn-primary text-white text-uppercase" type="submit">Eliminar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@empty

@endforelse

@endsection