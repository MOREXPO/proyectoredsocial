@extends('layouts.app')

@section('content')
    {!! Form::open(['url'=>'/anuncios','method'=>'post','files'=>true]) !!}

    <table align="center" cellpadding="30">
        <tr>
            <td><h3>{!! Form::label('nombre','Titulo:') !!}</h3></td>
            <td>{!! Form::text('titulo') !!}</td>
        </tr>
        <tr>
            <td><h3>{!! Form::label('descripcion','Descripción:') !!}</h3></td>
            <td>{!! Form::textarea('descripcion', null, ['id' => 'descripcion', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}</td>
        </tr> 
        <tr>
            <td colspan="2"><h3>{!! Form::file('file') !!}</h3></td>
            @if(count($errors)>0)
            <div class="alert alert-danger">
                Upload Validation Error
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
            @endif
        </tr>
        <tr>
            <td colspan="2"><h3>{!! Form::submit('Publicar') !!}</h3></td>
        </tr>
    </table>
    {!! Form::close() !!}
@endsection