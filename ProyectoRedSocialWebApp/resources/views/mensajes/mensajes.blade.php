@extends('layouts.app')

@section('content')

<!-- Portfolio Section-->
<section class="page-section portfolio" id="portfolio">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Mensajes</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Portfolio Grid Items-->
        <div class="row">

            @forelse($mensajes as $mensaje)
            <div class="col-md-6 col-lg-4 mb-5">
                <div class="portfolio-item mx-auto" data-toggle="modal"
                    data-target="#portfolioModal{{$mensaje->idAnuncio}}">
                    <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                        <div class="portfolio-item-caption-content text-center text-white"><i
                                class="fas fa-plus fa-3x"></i></div>
                    </div>
                    <div class="img-fluid text-uppercase" alt="">
                        <strong>
                            @php
                            if($mensaje->idUsuarioEmisor==auth()->id()){
                            echo App\Http\Controllers\MensajeController::nombreUsuarioDestino($mensaje->idAnuncio);
                            }else{
                            echo App\Http\Controllers\MensajeController::nombreMensajero($mensaje->idUsuarioEmisor);
                            }

                            $imagenAnuncio=json_decode(App\Models\Anuncio::WHERE('id','=',$mensaje->idAnuncio)->get('foto'));
                            $imagenAnuncio=$imagenAnuncio[0]->foto;
                            @endphp
                        </strong>
                    </div>
                    <img class="img-fluid" src="images/{{$imagenAnuncio}}" alt="" />
                </div>
            </div>
            @empty

            @endforelse
        </div>
    </div>
</section>

<!-- inciarChat Modals-->
@forelse($mensajes as $mensaje)
<div class="portfolio-modal modal fade" id="portfolioModal{{$mensaje->idAnuncio}}" tabindex="-1" role="dialog"
    aria-labelledby="portfolioModal{{$mensaje->idAnuncio}}Label" aria-hidden="true" style="overflow-y: scroll;">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fas fa-times"></i></span>
            </button>

            <div class="modal-body text-center">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            {!! Form::open(array('route' => array('mensaje.store', $mensaje->idAnuncio))) !!}
                            <!-- Portfolio Modal - Title-->
                            <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0"
                                id="portfolioModal{{$mensaje->idAnuncio}}Label">
                                @php
                                if($mensaje->idUsuarioEmisor==auth()->id()){
                                echo App\Http\Controllers\MensajeController::nombreUsuarioDestino($mensaje->idAnuncio);
                                }else{
                                echo App\Http\Controllers\MensajeController::nombreMensajero($mensaje->idUsuarioEmisor);
                                }
                                @endphp
                            </h2>
                            <!-- Icon Divider-->
                            <div class="divider-custom">
                                <div class="divider-custom-line"></div>
                                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                <div class="divider-custom-line"></div>
                            </div>
                            <!-- Portfolio Modal - Image-->
                            <div class="img-fluid rounded mb-5" alt="">
                                @php
                                $mensajesAnuncio = DB::select('SELECT * FROM mensajes WHERE idAnuncio = ?',
                                [$mensaje->idAnuncio]);
                                @endphp
                                @forelse ($mensajesAnuncio as $mensajeAnuncio)
                                @if($mensajeAnuncio->idUsuarioEmisor==auth()->id())
                                <div class="cajaVerde"> {{$mensajeAnuncio->texto}}</div>
                                @else
                                <div class="cajaAzul"> {{$mensajeAnuncio->texto}}</div>
                                @endif
                                @empty

                                @endforelse
                            </div>
                            <!-- Portfolio Modal - Text-->
                            <p class="mb-5">{!! Form::textarea('texto', null, ['id' => 'texto', 'rows' => 4,
                                'cols' => 54, 'style' => 'resize:none']) !!}
                            </p>
                            {!! Form::submit('Enviar mensaje') !!}
                            @if(count($errors)>0)
                            <div class="alert alert-danger">
                                Upload Validation Error
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>
                                        {{$error}}
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@empty

@endforelse

@endsection