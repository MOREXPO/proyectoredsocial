<?php

use App\Models\Anuncio;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Auth::routes();
Route::get('/home', 'HomeController@inicio')->name('home')->middleware("auth");
Route::get("{idPublicador}/anuncios", "AnuncioController@showPublicador");
Route::get('/create', 'AnuncioController@create')->name('create');
Route::post('/anuncios', 'AnuncioController@store')->name('store');
Route::get('/mensajes', 'MensajeController@inicio')->name('mensajes')->middleware('auth');
Route::get('/favoritos', 'SeguirController@inicio')->name('favoritos')->middleware('auth');
Route::get('/mis-anuncios', 'AnuncioController@inicio')->name('mis-anuncios')->middleware('auth');
Route::post('/mensaje/{idAnuncio}', 'MensajeController@store')->name('mensaje.store');
Route::post('/seguimientos/{idAnuncio}', 'SeguirController@store')->name('seguir.store')->middleware('auth');
Route::delete('/eliminar-seguimiento/{idAnuncio}','SeguirController@destroy')->name('destroySeguimiento')->middleware('auth');
Route::delete('/eliminar-anuncio/{idAnuncio}','AnuncioController@destroy')->name('destroyAnuncio')->middleware('auth');
Route::get('/verificarSeguimiento/{idAnuncio}','SeguirController@verificar')->name('verificarSeguimiento')->middleware('auth');