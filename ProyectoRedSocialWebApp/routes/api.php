<?php

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::ApiResource('consultas','APIConsultasController');
Route::get('/validacion/{username}/{password}','APIConsultasController@Validacion');
Route::get('getAnunciosHome/{idUsuario}','APIConsultasController@getAnunciosHome');
Route::get('getMisAnuncios/{idUsuario}','APIConsultasController@getMisAnuncios');
Route::get('getMensajes/{idUsuario}','APIConsultasController@getMensajes');
Route::post('createUser/{name}/{username}/{password}/{provincia}','APIConsultasController@createUser');
Route::post('createAnuncio', 'APIConsultasController@createAnuncio');
Route::delete('deleteAnuncio/{idAnuncio}', 'APIConsultasController@deleteAnuncio');
Route::get('nombreMensajero/{idUsuarioEmisor}','APIConsultasController@nombreMensajero');
Route::get('nombreUsuarioDestino/{idAnuncio}','APIConsultasController@nombreUsuarioDestino');
Route::get('obtenerImagen/{idAnuncio}','APIConsultasController@obtenerImagen');
Route::delete('eliminarSeguir/{idUsuario}/{idAnuncio}', 'APIConsultasController@eliminarSeguir');
Route::post('agregarSeguir/{idUsuario}/{idAnuncio}','APIConsultasController@agregarSeguir');
Route::get('verificarSeguimiento/{idUsuario}/{idAnuncio}','APIConsultasController@verificar');
Route::get('anunciosFavoritos/{idUsuario}', 'APIConsultasController@anunciosFavoritos');
Route::get('getChat/{idAnuncio}', 'APIConsultasController@getChat');
Route::post('enviarMensaje/{idUsuario}/{idAnuncio}/{texto}','APIConsultasController@enviarMensaje');